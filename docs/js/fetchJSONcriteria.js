// from https://pqina.nl/blog/how-to-prompt-the-user-to-download-a-file-instead-of-navigating-to-it/
function downloadFile(file) {
    // Create a link and set the URL using `createObjectURL`
    const link = document.createElement("a");
    link.style.display = "none";
    link.href = URL.createObjectURL(file);
    link.download = file.name;

    // It needs to be added to the DOM so it can be clicked
    document.body.appendChild(link);
    link.click();

    // To make this work on Firefox we need to wait
    // a little while before removing it.
    setTimeout(() => {
        URL.revokeObjectURL(link.href);
        link.parentNode.removeChild(link);
    }, 0);
}

function fetchJSONcriteria() {
    let criteria = []
    document.querySelectorAll("span.criteria-data").forEach(criteriaData => {
        criteria.push(JSON.parse(criteriaData.innerText))
    })
    console.log(`found ${criteria.length} criteria`)
    const criteriaFile = new File([JSON.stringify(criteria)], "criteria.json");
    downloadFile(criteriaFile);
}
