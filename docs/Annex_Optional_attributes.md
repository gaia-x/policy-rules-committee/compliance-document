
The following defines the optional attributes that are suggested to be used to describe Participants, Services, and Resources.

## Participant

**Legal person**

| Attribute                                                           | Cardinality | Trust Anchor | Comment                                           |
|---------------------------------------------------------------------|------|-----------|---------------------------------------------------|
| [`parentOrganization[]`](https://schema.org/parentOrganization)     | 0..* | Gaia-X Registry | A list of direct `participant` that this entity is a subOrganization of, if any. |
| [`subOrganization[]`](https://schema.org/subOrganization)           | 0..* | Gaia-X Registry | A list of direct `participant` with a legal mandate on this entity, e.g., as a subsidiary. |


## Resource & Subclasses

**Resource**

| Attribute               | Card. | Trust Anchor | Comment                                         |
|-------------------------|-------|--------------|-------------------------------------------------|
| `aggregationOf[]`       | 0..*  | Gaia-X Registry        | `resources` related to the resource and that can exist independently of it. |
| `name`                  | 0..1     | Gaia-X Registry       | A human-readable name of the data resource |
| `description`           | 0..1     | Gaia-X Registry       | A free text description of the data resource |

**Physical Resource**

| Attribute              | Card. | Trust Anchor | Comment                                     |
|------------------------|-------|--------------|---------------------------------------------|
| `ownedBy[]`            | 0..*  | Gaia-X Registry        | a list of `participant` owning the resource. |
| `manufacturedBy[]`     | 0..*  | Gaia-X Registry       | a list of `participant` manufacturing the resource. |
| `location[].gps`       | 0..*  | Gaia-X Registry       | a list of physical GPS in [ISO 6709:2008/Cor 1:2009](https://en.wikipedia.org/wiki/ISO_6709) format. |

**Sustainability**

The following attributes have to be linked to a service offering, physical resource or legal person.  

| Attribute              | Card. | Trust Anchor | Comment                                     |
|------------------------|-------|--------------|---------------------------------------------|
| `powerUsageEffectiveness`       | 0..*  | `ownedBy`         |Power usage effectiveness (PUE) is a ratio that describes how efficiently a computer data centre uses energy; specifically, how much energy is used by the computing equipment (in contrast to cooling and other overhead that supports the equipment). PUE is the ratio of the total amount of energy used by a computer data centre facility to the energy delivered to computing equipment. The measurement is annually according to [ISO/IEC 30134-2:2016](https://www.iso.org/standard/63451.html) or [EN 50600-4-2](https://www.en-standard.eu/csn-en-50600-4-2-information-technology-data-centre-facilities-and-infrastructures-part-4-2-power-usage-effectiveness/).A decimal number equal or greater than 1.0 is expected. For data centres and server rooms with a planned IT capacity at or below 2MW, energy consumed for office space, ancillary building space, and general usage may be excluded from PUE calculations. For this case, exclusions have to be mentioned.|
| `waterUsageEffectiveness`       | 0..* | `ownedBy`        |Water usage effectiveness (WUE) is used to measure data centre sustainability in terms of water usage and its relation to energy consumption. It is calculated as the ratio between water used at the data centre (water loops, adiabatic towers, humidification, etc.) and energy delivered to the IT equipment. WUE will be measured using the category 1 site value, per [ISO/IEC 30134-9:2022](https://www.iso.org/standard/77692.html) standard.|
| `waterType`       | 0..*  | `ownedBy`        |Breakdown of the types of water used. Potable water is free from contamination and that is safe to drink or to use for food and beverage preparation and personal hygiene, in adherence to [ISO/IEC 30134-9:2022](https://www.iso.org/standard/77692.html).Freshwater is water having a low concentration of dissolved solids, in adherence to [ISO 14046:2016](https://www.iso.org/standard/43263.html). Greywater is wastewater with a low pollution level, no faecal matter, and reuse potential, in adherence to ISO 12056-1:2000. Blackwater is wastewater with significant pollution level without reuse potential, or recycled blackwater that has gone through tertiary treatment, in adherence to ISO 12056-1:2000.Seawater or brackish is water with significant salinity, in adherence to [ISO 14046:2016](https://www.iso.org/standard/43263.html).|
| `climateType`            | 0..* | `ownedBy`        | Cool climates are those that are at or below a cooling degree day measurement of 49.99 based on annual data in 2019 for the [NUTS 2 Region compiled by Eurostat](https://ec.europa.eu/eurostat/data/database?node_code=nrg_chddr2_a). Warm climates are those that are at or above a cooling degree day measurement of 50.00 based on annual data in 2019 for the [NUTS 2 Region compiled by Eurostat](https://ec.europa.eu/eurostat/data/database?node_code=nrg_chddr2_a). |
| `energyConsumption`            | 0..1  | `ownedBy`        | In Megawatt-Hours (MwH). |
| `renewableEnergyAmount[]`     | 0..*  | Gaia-X Registry       | Renewable is defined as technologies identified as renewable under [Directive 2009/28/EC](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX%3A32009L0028&from=EN) and carbon-free energy means any type of electricity generation from wind, solar, aerothermal, geothermal, hydrothermal and ocean energy, hydropower, biomass, landfill gas, sewage treatment plant gas, biogases, nuclear power, and carbon capture and storage. Renewable energy is measured based on the Renewable Energy Factor defined by [CSN EN 50600-4-3](https://www.en-standard.eu/csn-en-50600-4-3-information-technology-data-centre-facilities-and-infrastructures-part-4-3-renewable-energy-factor/); or a company may also measure renewable energy or carbon-free energy based on a publicly available methodology; or a company may measure renewable energy or carbon-free energy based on a published third party methodology, such as Green-e, RE100 or the Greenhouse Gas Protocol. Renewable energy can be measured at the facility, country, or company portfolio level within the Member States of the European Union. Claims should be proven by certificates (e.g. with a power purchase agreement (PPA), Guarantee of origin (GoO) or Renewable Energy Certificate (REC) and amount in MWh). |
| `dataReplication` | 0..1  | `ownedBy`   | Amount of data replications for security, back-up and other reasons. |
| `lifetimeExpectation` | 0..1  | `ownedBy`   | List of lifetime expectations of meaningful components (datacentres, buildings ...) and the methodology used in the assessment. |
| `ecolabels[]`       | 0..*  | Gaia-X Registry        | Ecolabels of equipment e.g. from but not limited to TCO or Electronic Product Environmental Assessment Tool (EPEAT). |
| `energyBandwidthEfficiency`     | 0..*  | `ownedBy`        | Break down of the bandwidth / energy consumption. |
| `environmentManagementSystem[]` | 0..*  | Gaia-X Registry   | List of certificates covering the environment management and the exact scope of it (e.g. group-wide). The list might include ISO14001, ISO14040, ISO14044, Commission Delegated Regulation (EU) 2021/2139, and the CNDCP Auditing Framework to make data centres climate neutral by 2030.|
| `lifeCycleAssessment[]`       | 0..*  | Gaia-X Registry        | Description of life cycle assessment e.g. based on ITU L.1410 or ETSI 203 199 Methodology application. This Methodology is a complement to the ISO 14040 & 14044 for environmental life cycle assessments of information and communication technology goods, networks and services. Compliance to all requirements may not be possible deviations from the requirements shall be clearly motivated and reported. |


## Service & Subclasses

**Service offering**

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `name`                 | 0..1     | Gaia-X Registry       | A human readable name of the component |
| `aggregationOf[]`      | 0..*  | Gaia-X Registry       | a resolvable link to the `resources` self-description related to the service and that can exist independently of it. |
| `dependsOn[]`          | 0..*  | Gaia-X Registry        | a resolvable link to the `service offering` self-description related to the service and that can exist independently of it. |
| `(access control, throttling, usage, retention, ...) |
| `dataProtectionRegime[]`| 0..* | Gaia-X Registry       | a list of data protection regimes from the list available below |

**Personal Data Protection Regimes**
Gaia-X strives to contribute compliance with the protection of personal data, i.e., privacy. 
Gaia-X envisages its implementation internationally, whilst not neglecting its European sources. 

Conformity with specific requirements deriving from different regimes protecting personal data will be subject to respective Labels. 
Nonetheless, it is considered significant added value, to allow Customers to filter Service Offerings by applicable regimes. 

Thus, Service Offerings shall be able to identify Personal Data Protection Regimes in their respective Self Description. Such identified regimes do not entail any trusted or verified statement of compliance. 

Non-exclusive list of Personal Data Protection Regimes:

- `GDPR2016`: [General Data Protection Regulation](https://eur-lex.europa.eu/eli/reg/2016/679/oj) / EEA
- `LGPD2019`: [General Personal Data Protection Law](http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2018/lei/l13709.htm) (_Lei Geral de Proteção de Dados Pessoais_) / BRA
- `PDPA2012`: [Personal Data Protection Act 2012](https://sso.agc.gov.sg/Act/PDPA2012) / SGP
- `CCPA2018`: [California Consumer Privacy Act](https://oag.ca.gov/privacy/ccpa) / US-CA
- `VCDPA2021`: [Virginia Consumer Data Protection Act](https://lis.virginia.gov/cgi-bin/legp604.exe?212+ful+CHAP0036+pdf) / US-VA

The participant may wish to refer to ISO/IEC 27701 as a tool for comparing the various data protection regimes.

To enable interoperability, it is strongly recommended to refer to the options provided in the [Gaia-X Registry](https://registry.gaia-x.eu). The Gaia-X Registry will allow for the possibility to select one or several of such regimes, as well as the possibility to add additional regimes. Maintenance of the registry shall regularly assess the individually identified regimes and - where appropriate - add such regimes to the pre-defined list of Personal Data Protection Regimes. 


**Service Instance / Instantiated Virtual Resource**

*Service Access Point*

A service access point is an identifying label for network endpoints used in the [OSI model](https://en.wikipedia.org/wiki/OSI_model). 

The format below doesn't represent all possible service access point types.

| Attribute   | Card. | Trust Anchor | Comment                    |
|-------------|-------|--------------|----------------------------|
| `name`      | 0..1     | Gaia-X Registry        | name of the endpoint       |
| `host`      | 0..1     | Gaia-X Registry        | host of the endpoint       |
| `protocol`  | 0..*     | Gaia-X Registry        | protocol of the endpoint   |
| `version`   | 0..1     | Gaia-X Registry        | version of the endpoint    |
| `port[]`    | 0..*  | Gaia-X Registry        | port of the endpoint       |
| `openAPI`   | 0..*     | Gaia-X Registry        | URL of the [OpenAPI documentation](https://github.com/OAI/OpenAPI-Specification/blob/3.1.0/versions/3.1.0.md) |

## Specific Services 

The following describes optional syntax recommendations.

**Verifiable Credential Wallet**

A `wallet` enables to store, manage, present Verifiable Credentials:
<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

| Attribute                            | Card. | Trust Anchor | Comment                                         |
|--------------------------------------|-------|--------------|-------------------------------------------------|
| `verifiableCredentialExportFormat[]` | 1..*  | Gaia-X Registry        | a list of machine readable formats used to export verifiable credentials. |
| `privateKeyExportFormat[]`           | 1..*  | Gaia-X Registry        | a list of machine readable formats used to export private keys. |

**Interconnection**

An `interconnection` enables a mutual connection between two or more elements.

| Attribute                               | Card. | Trust Anchor | Comment                                         |
|-----------------------------------------|-------|--------------|-------------------------------------------------|
| `location[].countryCode`                | 2..*  | Gaia-X Registry       | a list of physical locations in ISO 3166-2 alpha2, alpha-3 or numeric format with at least both ends of the connection. |

**Catalogue**

A `catalogue` service is a subclass of `serviceOffering` used to browse, search, and filter services and resources.

| Attribute                    | Card. | Trust Anchor | Comment                                         |
|------------------------------|-------|--------------|-------------------------------------------------|
| `getVerifiableCredentialsIDs`| 1..*  | Gaia-X Registry        | a route used to synchronize catalogues and retrieve the list of Verifiable Credentials (`issuer`, `id`). [1] |

[1]: Using the Verifiable Credential terms, it's up to the `issuer` to control and decide if the credential's [`id`](https://www.w3.org/TR/vc-data-model/#identifiers) can be dereferenced and the `holder` to implement access control of the verifiable credentials.

## Interoperability, Portability, Switchability and Intellectual Property Protection (experimental)

While the format of the attributes is not yet defined, the sections below give insights about the future requirements.

**Interoperability**

This covers how the service offering can **communicate** with another service, such as making or responding to requests.

The aim of the future attributes is to provide:

- a list of agreements in place with other service providers to ensure interoperability between systems
- a list of URL to interoperability capabilities and transparency documentation

See [ISO/IEC 19941:2017](https://standards.iso.org/ittf/PubliclyAvailableStandards/c066639_ISO_IEC_19941_2017.zip)-5.2.1 for a description of the facets that need to be addressed.

**Data Portability**

This refers to the porting of **data** (structured or unstructured) from one cloud service to another, public or private.

Note: Data portability will inevitably be limited to Customer Data and a _subset_ of Derived Data as determined by contract, privacy regulations, etc. See the `ServiceOffering.dataAccountExport` attribute.

The aim of the future attributes is to provide:

- a list of publicly filed declarations of adherence such as [SWIPO Code of Conduct](https://swipo.eu/current-swipo-code-adherences/)

If a Gaia-X service offering includes multiple cloud services with different data portability capabilities, it is expected to have separate Self-Descriptions for each type of data portability capability.

Note 2: The SWIPO codes of conduct do not cover all cloud service categories (e.g. there is no specific SWIPO Code for PaaS), so it is not possible to mandate this attribute for all services.

See [ISO/IEC 19941:2017](https://standards.iso.org/ittf/PubliclyAvailableStandards/c066639_ISO_IEC_19941_2017.zip)-5.2.2 for a description of the facets that need to be addressed.

**Application Portability**

This refers to the porting of customer or third party **executable code** from one cloud service to another, public or private.

Note: Application portability is highly complex, especially when it comes to identity management, licenses, access control lists, privacy controls, transfer of credentials, etc.

The aim of the future attributes is to provide:

- a list of URL pointing to a declaration of adherence such as [SWIPO Code of Conduct](https://swipo.eu/current-swipo-code-adherences/)
- a list of URL pointing to a declaration of whether and how the application license can be switched to another service provider

If a Gaia-X service offering includes multiple cloud services with different data portability capabilities, it is expected to have separate Gaia-X Credentials for each type of data portability capability.

Note 2: For [SWIPO Code of Conduct](https://swipo.eu/current-swipo-code-adherences/):

- This can apply to executable code within an IaaS virtual machine instance, or code that can be executed within a larger SaaS application (such as scripts or SQL stored procedures).

- This code of conduct does not cover all cloud service categories (e.g. there is no specific SWIPO Code for PaaS), so it is not possible to mandate this attribute for all services.

See [ISO/IEC 19941:2017](https://standards.iso.org/ittf/PubliclyAvailableStandards/c066639_ISO_IEC_19941_2017.zip)-5.2.3 for a description of the facets that need to be addressed.

**Switchability**

This refers to moving a customer's **account** from one service provider to another, including the change of contracts and necessary technical changes.

Service switching is highly complex. Moving a customer to another cloud application provider requires identifying a destination which is in the same broad category of cloud service offering, and where the common functionality offered by the original service and the destination meets the minimum business needs of the customer wishing to switch. Since all services evolve constantly this is a moving target, and each customer will have different criteria. In addition, there can be considerable legal issues to be addressed. This includes the "policy" facets of data and application portability as described in ISO/IEC 19941:2017, but also the unique specifics of contracts and any possible implications for security and this continued compliance with privacy and data protection regulations. Responsibility for each stage of the switching process (including security, liability, and privacy compliance) will need to be determined and agreed upon in advance by all parties.

The aim of the future attributes is to provide:

- a list of URL pointing to a declaration of whether and how a cloud service contract can be switched to another service provider
- a list of URL pointing to required or available policies for switching
- a list of URL pointing to a declaration of whether and how the Identity and Access Management function can be switched to another service provider

Note 1: At present, there do not seem to be any established Codes of Conduct or other frameworks that cover these aspects.

<!-- Note 2: Not all cloud services provide Identity and Access management, so it is not possible to mandate this attribute. -->

<!--
| 1.x     | `virtualLocation[]`  | 1..*  | Gaia-X Registry        | a list of locations such as an Availability Zone, network segment, IP range, AS number, ... (format to be specified in a separate table) |
-->

**Intellectual Property Protection**

The term Intellectual property (IP) refers to unique and value-adding creations of the human mind. IP enables the granting of property-like rights over new discoveries, new knowledge and creative expressions. IP relates to but is not limited to know-how, products, processes, software, data, and artistic works. The rights resulting from various forms of IP are referred to as intellectual property rights (IPR). IPRs provide ownership, i.e. the capacity to grant the IP usage rights (licensing), to assign the IP and rights to exclude third parties from using what is protected by the owner. Trust is paramount in the willingness of partners to collaborate and share information. IPRs can ensure the confidence of the partners that their knowledge capital and investment will be respected in the frame of acknowledged context and therefore that the use of the information shared will remain in the agreed context for the agreed duration. IPRs can exist in the form of a Patent, Utility model, Copyright, Trademark, Industrial design, Geographical Indication or Trade Secret (see [ WIPO](https://www.wipo.int/edocs/pubdocs/en/wipo_pub_895_2016.pdf) and [ISO 56005:2020(en)](https://www.iso.org/obp/ui/#iso:std:iso:56005:ed-1:v1:en))
 
In the context of the Gaia-X Trust Framework, IP can be declared on `Service Offering`, `PhysicalResource`, `SoftwareResource` and / or `DataResource` level.


| Attribute                    | Card. | Trust Anchor | Comment                                         |
|------------------------------|-------|--------------|-------------------------------------------------|
| `intellectualPropertyOwner[]`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]`        | An IP owner is a person or organisation that has the ownership and has the right to decide how the creation can be used by others. The attribute expects a list of owners either as a free form string or or URIs from which Self-Descriptions can be retrieved. |
| `intellectualPropertyOwnershipType`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]` | Declaration of the type of ownership type ([Patent](https://www.wipo.int/patents/en/), [Utility model](https://www.wipo.int/patents/en/topics/utility_models.html), [Copyright](https://www.wipo.int/copyright/en/), [Trademark](https://www.wipo.int/trademarks/en/), [Industrial design](https://www.wipo.int/designs/en/), [Geographical Indication](https://www.wipo.int/geo_indications/en/) and [Trade Secret](https://www.wipo.int/tradesecrets/en/)) according to WIPO. |
| `iPRegistrationProof[]`| 0..*  | Gaia-X Registry        | Legally binding proof that verifies the registration of the declared ownership type. The proof can be preferably provided by an URL. <br> Sources might include: <br>- Patent: [global Patentscope for WIPO](https://patentscope.wipo.int/search/en/search.jsf) and [Espacenet for EU](https://www.epo.org/searching-for-patents/technical/espacenet.html) <br/>- Trademark: [Brand name database](https://branddb.wipo.int/en/quicksearch?by=brandName&v=&rows=30&sort=score%20desc&start=0&_=1681826804155) (WIPO); [eSearch Plus at EU level](https://www.epo.org/searching-for-patents/technical/espacenet.html) and [TM view](https://www.tmdn.org/tmview/#/tmview) for all EU countries at national level <br/>- Industrial design: [Global design database](https://www3.wipo.int/designdb/en/index.jsp) and at EU level [Design view](https://www.tmdn.org/tmdsview-web/welcome#/dsview) <br/>Alternatively, a certificate has to be provided. <br/> In case of a Trade Secret, a NDA/Confidentiality Agreement or IPR Agreement can be provided. <br/> If none of the above solutions can be made available, a self-claim must be provided.
| `iPExpirationDate`| 0..1  | Gaia-X Registry        | Date expected in the ISO 8601 format. The terms of coverage can be cross-checked with the WIPO [Terms of Protection](https://www.wipo.int/export/sites/www/standards/en/pdf/archives/03-09-02arc2008.pdf) |
| `geographicValidity[]`| 0..*  | Gaia-X Registry        | List of the Countries which are covered by `iPRegistrationProof[]` in ISO 3166-2 alpha2, alpha-3 or numeric format. Patent under EU Unitary Patent System (from 1 June 2023) valid in contracting States. Patents under the [EU Unitary Patent System](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2012:361:0001:0008:en:PDF) will be from 1 June 2023 valid in [contracting States](https://www.epo.org/applying/european/unitary/unitary-patent.html).   |
| `usageRightsAgreements[]`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]`        | Description of the usage rights allowing a person or organisation the right to use the intellectual property. Such an agreement may be limited in time and context of the usage.  <br> The [WIPO Green licensing checklist](https://www3.wipo.int/wipogreen/docs/en/wipogreen_licensingchecklist_061216.pdf) and [EU factsheet](https://op.europa.eu/en/publication-detail/-/publication/e510929d-f015-11eb-a71c-01aa75ed71a1/language-en/format-PDF/source-227478787) on licence agreement helps to identify the issues that might be encountered in negotiating an agreement (contract) that relates to intellectual property and technology.|
| `usageRightsExpirationTime`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]`        | A date time in ISO 8601 format after which `usageRightsAgreement[]` is expired.|
| `usageRightsExpirationCondition[]`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]`        | A list of conditions after which usageRightsAgreement[] is expired. The Conditions can be expressed using ODRL. |

 
**Consistency Rules**
- For every declaration, a unique ID (`intellectualPropertyID`) will be automatically generated and assigned. All other declared attributes in the section have to reference that ID.
- The declaration of IP can be on service level (identifiable through the name and `providedBy`) and/or resource level (identifiable through `ownedBy[]` and `producedBy[]`). The `intellectualPropertyID` has to link to one or multiple unique `Service Offering`, `PhysicalResource`, `SoftwareResource` and/or `DataResource`. 
- If IP is wished to be declared, `intellectualPropertyOwner[]`, `intellectualPropertyOwnershipType[]`, `iPRegistrationProof[]`,`iPExpirationDate`, `geographicValidity` and `usageRightsAgreements[]` are mandatory.
- `usageRightsExpirationCondition` and `usageRightsExpirationTime` have to be linked to one or multiple usageRightsAgreements within `usageRightsAgreements[]`.


