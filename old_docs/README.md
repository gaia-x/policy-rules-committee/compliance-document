# Gaia-X Policy Rules and Labelling Document

## Publisher
Gaia-X European Association for Data and Cloud AISBL
Avenue des Arts 6-9
1210 Brussels
www.gaia-x.eu

## Authors
Gaia-X Policy Rules Committee
Gaia-X Working Group Policy Rules Document
Gaia-X Working Group Labelling
Gaia-X Working Group Trust Framework
Gaia-X Technical Committee

## Contact
E-mail: prld-document@gaia-x.eu

## Copyright notice
©2021 Gaia-X European Association for Data and Cloud AISBL

This document is protected by copyright law and international treaties. You may download, print or electronically view this document for your personal or internal company (or company equivalent) use. You are not permitted to adapt, modify, republish, print, download, post or otherwise reproduce or transmit this document, or any part of it, for a commercial purpose without the prior written permission of Gaia-X European Association for Data and Cloud AISBL. No copying, distribution, or use other than as expressly provided herein is authorized by implication, estoppel or otherwise. All rights not expressly granted are reserved.

Third party material or references are cited in this document.