# Gaia-X Labels Operational Conditions Clarifications


# Table of contents
0. [Defintions](#Definitions)
1. [Scope of the document](#Scope_of_the_document)
2. [Scope of the labelling](#Scope_of_the_labelling)
3. [Owner of the label](#Owner_of_the_label)
4. [Principles for issuers of the labels](#Principles_for_issuers_of_the_labels)
5. [Methodology](#Methodology)
6. [Becoming an Evaluation Body (EvaB)](#Becoming_an_Evaluation_Body)
7. [Labels validity](#Labels_validity)
8. [Update cycles – material and rules of the game](#Update_cycles–material_and_rules_of_the_Game)
9. [Legal framework – use, revocation, maintenance of the label (issuer - labelled)](#Legal_Framework–Use_Revocation_Maintenance_of_the_label)
10. [Legal framework - Gaia-X and issuer](#Legal_Framework_Gaia-X_and_Issuer)
11. [IP](#IP)
12. [Versioning](#Versioning)

## 0. Definitions <a namen="Definitions"></a>
TO BE ADDED APPLICABLE TERMINOLOGY  

## 1.Scope and Purpose of the document <a name="Scope_of_the_document"></a>
In the scope of the procedures to issue Gaia-X labels following the Gaia-X Labelling Framework this document's purpose is to contribute towards the following two steps: 
1. it shall act as a reference point to start an initial consultation with external entities and existing CABs, be it certification or auditing bodies or monitoring bodies, to identify the useability and practicability of the Gaia-X Labelling Framework, 
2. it shall define terms, roles, and related procedures to perform relevant assessment and thus lay a foundation for the Gaia-X Labelling Framework.

### Re 1
Gaia-X has developed the material requirements of its Labelling Framework, and will continue further optimizing its contents. Gaia-X, as of now, remains lacking a correlating framework addressing the procedrual aspects on the actual assessment and the subordinate "rules of the game". Given the complexity of a meta-approach by Gaia-X, this will require alignment with existing bodies to ensure that the expectations by Gaia-X can be operationalised in due time. 
This document shall serve as the basis for this internal consultation phase and after being adjusted where necessary become the reference description for terms, roles, and related procedures. 

### Re 2
By defining central terms, this document shall streamline applicable terminology and thus ensure consistent application of the Labelling Framework. 
Roles shall be defined to ensure consistency in the the involved parties and their obligations and powers. This shall, e.g., covers aspects such as Owner, Issuer, Verifier/Validator and Holder. 
This document shall - by principle - relate to the main objectives and act as a first point of reference. Where further detailed procedures will be necessary - e.g., in regards of the appointment and withdrawal of bodies and or their related roles. E.g., the latter shall contain principles and requirements for the competence, impartiaility and conistency of bodies providing assessemnts with regards to the Gaia-X Labelling Framework.

## 2.Scope of the labelling <a name="Scope_of_the_labelling"></a>

#### i.Carve-out
text



## 3.Owner of the label <a name="Owner_of_the_label"></a>

Label Owners are entities that decide to define a specific Label for their business. These can be Service Providers, Service Users or Governmental Authorities, Standardisation Authorities, Trade Associations, Industrial Associations, etc. 



#### i.“Gaia-X” labels 
Gaia-X association is owner of one label called “Gaix-X label” (name to be defined)  which has 3 levels.

ii . Domain specific labels

In general, Gaia-X aim is to have the more general rules as possible, in order to have its label widely adopted by providers and users. However and by exception, Gaia x can also decide to be owner of domain specific label. The financial sector could for example decide that they add some rules to the Gaia-X label in order to have a "Gaia-X  financial label". 
More over, other association or entities could decide as explained in the labelling framework document  to use the Gaia x system to develop labels (see paragraph 6). In this example, the Banking Association defines the name of the Label and specifies the requirements to be verified (e.g., Territorial Jurisdiction, service location, required certifications, etc.). The Label Owner engages the Gaia-X Association to develop the Label and issue it and must receive the agreement of the AISBL formally."


#### iii.Consistency of labels, inheritance and reciprocity of conformity
text

## 4.Principles for issuers of the "Gaia x label" <a name="Principles_for_issuers_of_the_labels"></a>
The "Gaia-X Labelling Criteria" document defines criteria and an associated list of "Conformity Assessment Bodies" (CAB). The claims for compliance to the Labeling Criteria are part of the "Self Description" (SD) of the Provider's Service Offering. Each claim will ultimately be included in a verifiable credential (VC) which provides the electronic proof that a CAB has verified compliance to the specific criteria (which may include a translation service from other formats to VC (Recommended formats for electronic proof are: 

- W3C Verifiable Credentials
- PDF with XMP metadata following the ISO 16684-1:2019 standard)

The Issuance of a Label is the result of a process determining that 
* all claims and VC are validated against the Gaia-X labelling catalogue (e.g. has each claim needed to achieve a given Gaia-X Label been verfied by a trusted CAB and is the VC as proof part of the self description available). The consumer has full visibility to which CAB has validated each criterion.
* After successful validation the Label Issuer creates one additonal VC which verifies that all label critera are met

Label issuers are entities that are legally responsible for surveillance, evaluating the continued conformity of the service to the specified requirements sufficient to assure continued confidence in the labels.

The Gaia-X association is the Owner of the Gaia-X Label and may act as Label Issuer or delegate the operational and maintenance work to trusted entities (as defined in the Architecture Document). How to become such an entity is described in part 6 of this document.


## 5.Methodology <a name="Methodology"></a>

#### i.Types of evaluation 

##### 1.Three types of evaluation 
Generally, one distinguishes betweeen three types of evaluation (see e.g., ISO 17000:2020)
a. First-Party Conformity Assessment Activity (Self-Attestation)
b. Second-Party Conformity Assessment Activity (Peer-Review)
c. Third-Party Conformity Assessment Activity (Impartial Third-Party)

#### Object of Conformity
The object is the entity to which the compliance criteria apply. 
The general concept of Gaia-X addresses Service Offerings, thus the object shall refer to a Service Offering. However, for general principles such as security and risk management, evidence may be required at the provider level. 

##### 2.Retrospectiveness vs anticipating assurance 
To the extent the Gaia-X Labelling Framework is not buidling upon third-party standards, the assessment shall follow distinct procedures. 
Generally, one may distinguish between a 
a. retrospective assessment, assessing conformity over a defined period [ex-post] and
b. anticipating assurance, i.e., an assessment of facts that allow for a likelyhiood above a defined threshold that the object of conformity assessment is and will be conform [ex-ante].

##### 3.Management process vs assessments of defined criteria on a case by case basis
To the extent the Gaia-X Labelling Framework is not buidling upon third-party standards, the assessment shall follow distinct procedures. 
Generally, one may distinguish between a assessments addressing (management) processes and assessments addressing conformity to a defined criteria. 
Whilst the first approach ensures that there will be no systematic non-conformity but accepts that there might be individual failures of performance, the latter require full conformity in every single case. 


##### 4.Sampling?  
text
##### 5.Complaints against non-compliance  <a name="Complaints"></a>

1. The consumer of a compliant Gaia-X service has the right to lodge a complaint regarding the non-compliance of the service to the requirments defined in the Label which was issued to that service.

2. The issuers of a Label shall have a process in place to deal with compliants. This process shall conisder the opinions of all relevant parties in a fair, reasonable and timely manner. 

3. When a complaint is upheld, remedial action shall be requested along with a timeframe for its execution. Remidial action shall be realistc and proportionate, and in serious cases may result in revocation of the Label.

4. Any involved party has the right to appeal any decision made during the complaint process. The appeals process is defined ((here-TBD)).

5. The complaints process shall be carried out by stakeholders not involved in the decision to issue a label and may be carried out by a third party other than the issuer of the Label, provided it has been accredited.


#### ii.Expected evaluation results and statements
text
#### iii.Process for a service to be labellised
1- For each service it wants to be labellised,  the service provider chooses a CAB that has been accredited by Gaia x ( see part 6)  and who will the label issuer
2- for each criteria, a verifiable credential is issued 
2-  The CAB issues a final verifiable credential  ensuring that all the criteria are met and with this final verifiable credential issues the label. 
#### iii.Process for a service to be labelled

The Labelling Criteria Document specifies the criteria for each label and the potential CABs which can certify compliance to the individual criteria.
1. For each service offering that a service provider wants to become labelled,  the service provider chooses a CAB that has been accredited by Gaia-X (see part 6)  to issue the intended Gaia-X label. That CAB then becomes the label issuer of the Gaia-X label.

2. For each criteria relevant for the intended label the Gaia-X label issuer 
    1. either verifies that the set of attributes relating to that criteria are already covered by a valid VC issued by a CAB (can be different from the one that issues this Gaia-X label) accredited by Gaia-X for that set of of attribute,
    2. or checks that the criteria is met.

3.  If all criteria relevant for the intended label are either already correctly attested for by other CABs (case 2.2) or been checked by the label issuing CAB itself (case 2.2), then the CAB issues a final verifiable credential (VC) covering all relevant attributes and thereby states that all the criteria are met. 
With this final verifiable credential the Gaia-X label is issued. 

The final decision to award a label or not is therefore taken by the issuer of the label through the issuance of the overarching verifiable credential.

#
## 6.Becoming an Evaluation Body (EvaB) <a name="Becoming_an_Evaluation_Body"></a>

#### i.General material requirements
All CABs performing assessments in the context of Gaia-X must meet recognized standards in regards of relevant aspects such as impartiality, expertise of the subject matter, fairness and transparency. 
CABs may in particular prove their suitability by accredited for [ISO17065] respectively [ISO17021], complemented by the requirements defined in the respective annexes, being an accredited monitoring body under GDPR pursuant Art. 41, the  International Code of Ethics for Professional Accountants (including International Independence Standards) [IESBA Code], or equivalents, especially if provided by applicable laws. 
The requirements will define several profiles corresponding to the various roles in the conformity assessments, in order to allow CABs that only perform a subset of the conformity assessment activities, in particular those that only perform evaluation activities.
The technical competence requirements associated to accreditation are sufficient to perform conformity assessments at levels 1 and 2. However, advanced competences are required in order to perform a conformity assessment at level 3. As a consequence, conformity assessment bodies shall be authorised by the national cybersecurity certification authority to carry out in the context of an evaluation at level 3 conformity assessment tasks related to highly technical topics including:
 Penetration testing, including the design and performance of penetration tests and the analysis of penetration testing activities performed by a CSP or its contractors.
 Analysis of development activities, and in particular the review of the design and implementation of security measures by the CSP.
##### 1.legal entity, etc. 
text
##### 2.Expertise in subject matter 
text
##### 3.Impartiality
text
##### 4.Tracibility and transparency 
text
##### 5.Confidentiality 
text
##### 6.Comparability of findings 
text
##### 7.etc. 
text
##### 8.audit the auditor 
text

#### ii.Additional formal requirements

##### 1.Accreditation against specific evaluation standards
text
##### 2.Accreditation by specific national or European bodies (e.g., NCCA, Data protection authority, etc. ) 
text

## 7.Labels validity <a name="Labels_validity"></a>

#### i.Lifetime

The minimum validity period of all Labels awarded under this framework is set at 1 calendar year from the date of issue. Bodies that issue labels may set a longer validity period, which shall not be longer than 3 years. 

#### ii.Maintain compliance

A service shall maintain compliance to the requirments defined in the rules of the Label that was awarded to that service during the validity period of the Label. The provider shall ensure this continuous compliance.

#### iii.Termination process

An awarded label is valid until either:

1. The end of it's validity period (automatically), or
2. The provider voluntarily revokes the label, or
3. The label issuer revokes  the label as a result of a [complaint](#Complaints), or
4. Legislation comes into effect that invalidates one or more requirements in the label rules which causes non-conformance to that legsilation. This will affect all the services of all providers that have been awarded the same Label.

The termination process will be automated as much as possible by the Gaia-X compliance services. If a revokation is triggered manually, that trigger will be processed by the compliance services in an auditable manner.

#### iv.Renewal

There is no automated renewal of a label after termination. Upon termination of a label, the provider may seek to renew the label for the given service. Renewal shall be against the latest version of the label criteria and the requirements outlined therein. If the requirments are materially the same as the previous label, this should not be an onerous process. A provider shall anticipate the renewal process and should aim to ensure that a service always has a valid label.

#### v.Transition period

When the revisions of the PRLD impact one or more of the labels, or when the label requirements themselves have changed, the providers will have the choice of adapting their compliance to the revised requirements or remaining qualified under the former requirements, for a maximum duration of twelve (12) months from the entry into force of the revised label requirements.  

Exceptionally, in case of changes that have become appropriate under applicable laws or standards impacting the PRLD or the label requirements, the Gaia-X Board can determine a grace period that deviates from the maximum twelve (12) months term, when relevant in view of the applicability of the changes of the applicable laws or standards.  

#### vi.Consistency and scalability with other standards

The intent is always to keep the label criteria and the issued labels consistent and compliant with other applicable standards. This may lead to deviations when any of such standards are updated and their defined transition or grace periods. Such deviations should be limited in time. Therefore the Gaia-X PRC will take action when applicable standard changes apply.

## 8.Legal Framework – use, revocation, maintenance of the Label (issuer - labelled) <a name="Legal_Framework–Use_Revocation_Maintenance_of_the_label"></a>

#### i. Required terms and conditions, obligations of involved parties
There shall be general agreements (terms and conditions) between the involved parties. The involved parties may be expected Label Holder, the Gaia-X AISBL, the entity/assessor providing an assessment report respectively attestation towards the Label Issuer, or the Label Issuer. 
Additionally, this document shall define general obligations of the parties.

##### 1) The terms and conditions between the Cloud Service Provider and the Label Issuer shall ensure that
the Cloud Service Provider will submit information according to the requirements defined in this document. E.g., this can relate to the requirement of providing attestation reports as verifiable credential.
the Cloud Service Provider will, where necessary, indicate the distinct criteria to which a claim or related attestation applies


##### 2) General Obligations of Cloud Service Provider 
For purpose of the assessment and - where applicable - ongoing monitoring by the CAB, the CSP shall - without jeopardizing the security or confidentiality of its Service Offering - 
o cooperate in good faith with the CAB
o grant access to additional information that the CAB may request; 
o give necessary access to personnel, including management, within the Cloud Service Provider from whom the CAB determines it may be necessary to obtain evidence relevant to the assessment;

Once the Cloud Service Provider holds a label, the Cloud Service Provider shall
o inform the CAB of major changes to Service Offering that may have an impact on label; 


##### 3) Obligations of the CAB issueing the label
CABs issueing the label shall 
o take appropraite actions where they become aware of facts that might adversely affect the eligibility of the Cloud Service Provider to keep holding the Label

##### 4) Obligations of the CAB performing the assessments
CABs performing the assessments shall 
o comply with the third-party standards requirements of acting as a CAB to such standards
o where applicable, ongoingly monitor compliance of the Cloud Service Provider to such third-party standard
o take applicable take appropriate actions where the Provider does not comply with the respective third-party standards; in cases where such actions lead to a withdrawal of a positive attestation and such attestation was translated into a verifiable credential following Gaia-X requirements, the CAB shall - in accordance with the Gaia-X requirements applicable to verifiable credentials - ensure that the necessary actions as defined in those requirements. 

#### iii.License agreement owner-issuer
Gaia-X will file trademarks under its own name protecting the marking to be made available for each Label and that will be offered under a trademark license to any provider under the following conditions: 
-	That such Provider is eligible to the label that corresponds with the respective trademark 
-	That such license shall be granted with a term consistent with the labelling procedure and that such license shall immediately terminate once the provider is no longer eligible to the Label 
-	Others customary provisions 
 
Such license will be granted directly by Gaia-X or through a proper licensing arrangement with a Label Issuer, with the right to enforce such trademark to be reserved by Gaia-X or made available as well to such Label Issuer. 


##### 1.Possible IP registration 

##### 2.Regional scope (probably worldwide)
text

## 9.Legal framework - Gaia-X and issuer <a name="Legal_Framework_Gaia-X_and_Issuer"></a>
An agreement shall be set up between Gaia-X and any issuer in order for such issuer to provide the Labelling services consistently with what is described in the procedures and requirements described in this document. In addition, such agreement shall address the following topics: IP licensing, chain of powers, chain of rules of the game, export control. 

#### i.IP licensing
text
#### ii.“Chain” of powers
text
#### iii.“Chain” of rules of the game
text

## 10.IP <a name="IP"></a>

#### i.Export control
text

## 11.Versioning <a name="Versioning"></a>

#### i.Gaia-X Operational Handbook versioning convention
The Gaia-X Operational Handbook defines the versioning of documents issued by the association. In the Handbook section 11.9.	(Document Change Classes and Versioning) the definition of change for a version and its corresponding descriptions are specified.
