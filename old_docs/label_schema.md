# Label schema

The section is mapping the high level requirements from the [labels](policy_rules_labeling_document.md) chapter with the low level requirements of [Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/) document in order to make them auditable.

## Baseline

The Gaia-X Compliance [engine implementation](https://gitlab.com/gaia-x/lab/compliance):

- verifies the format of the information.
- validates the cryptographic integrity of the information.
- validates the chain of certificate when available.
- validates the rules described below based an [open-world assumption](https://en.wikipedia.org/wiki/Open-world_assumption#Semantic_Web_languages).

Unless stated differently in the criteria, the Gaia-X Compliance doesn't verify if the credential issuer is or provides the object of the claims in the credential.  
It means that the Gaia-X Compliance allows any valid issuer to describe objects - resources or services - without having any legal, technical or operational relation with the object being described.  
This behavior is to avoid a lock-out effect from service providers which can or don't want to provide the descriptions of their services used as dependencies by other service providers.

It's up to the Label criteria to specify if additional constrains must be validated as part of the Label criteria implementation.

## Contractual governance

### Criterion P1.1.1

**The Provider shall offer the ability to establish a legally binding act. This legally binding act shall be documented.**

The Gaia-X Compliance is **mandating** **declaration** of:

- term&conditions and policy information on the Service Offering credential.

### Criterion P1.1.2

**The Provider shall have an option for each legally binding act to be governed by EU/EEA/Member State law.**

*To be implemented in a next release*

### Criterion P1.1.3

**The Provider shall clearly identify for which parties the legal act is binding.**

The Gaia-X Compliance is **mandating** **noterised declaration** of:

- the identification of legal participant using their unique registered business identifier.

### Criterion P1.1.4

**The Provider shall ensure that the legally binding act covers the entire provision of the Service Offering**

The Gaia-X Compliance is **mandating** **declaration** of:

- term&conditions and policy information on the Service Offering credential.

The Gaia-X Compliance is **enabling** **attestation** of:

- the Service Offering provider to detail additional Service Offering as dependencies

## General material requirements and transparency

### Criterion P1.2.1

**The Provider shall ensure there are specific provisions regarding service interruptions and business continuity (e.g., by means of a service level agreement), Provider’s bankruptcy or any other reason by which the Provider may cease to exist in law.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P1.2.2

**The Provider shall ensure there are provisions governing the rights of the parties to use the service and any Customer Data therein.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P1.2.3

**The Provider shall ensure there are provisions governing changes, regardless of their kind.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P1.2.4

**The Provider shall ensure there are provisions governing aspects regarding copyright or any other intellectual property rights.**

The Gaia-X Compliance is **enabling** **attestation** of:

- the issuer to detail the resources and dependencies of the Service Offering.

The Gaia-X Compliance is **mandating** **declaration** of:

-  the copryright information on virtual resources, ie a dataset, a software, a configuration file, an AI model. ...

### Criterion P1.2.5

**The Provider shall declare the general location of physicals Resources at urban area level.**

The Gaia-X Compliance is **enabling** **attestation** of:

- the issuer to detail the resources and dependencies of the Service Offering.

The Gaia-X Compliance is **mandating** **declaration** of:

-  the country and administrative area on physical resources, ie a datacenter, a bare-metal service, a warehouse, a plant

### Criterion P1.2.6

**The Provider shall explain how information about subcontractors and related Customer Data localization will be communicated.**

*To be implemented in a next release*

### Criterion P1.2.7

**The Provider shall communicate to the Customer where the applicable jurisdiction(s) of subcontractors will be.**

*To be implemented in a next release*

### Criterion P1.2.8

**The Provider shall include in the contract the contact details where Customer may address any queries regarding the Service Offering and the contract.**

*To be implemented in a next release*

### Criterion P1.2.9

**The Provider shall adopt the Gaia-X Trust Framework, by which Customers may verify Provider’s Service Offering.**

The Gaia-X Compliance is **mandating** **declaration** of:

- a service offering description to identify its provider as descripbed in [Criterion P1.1.3](#criterion-p113).


### Criterion P1.2.10

**The Provider shall provide transparency on the environmental impact of the Service Offering provided**

The Gaia-X Compliance is **enabling** **attestation** of:

- sustainability information on service offering and resources.

## Technical compliance requirements

### Criterion P1.3.1

**Service Offering shall include a policy using a common Domain-Specific Language (DSL) to describe Permissions, Requirements and Constraints.**

The Gaia-X Compliance is **mandating** **attestation** of:

- service offering and resources policies to be express in [ODRL](https://www.w3.org/TR/odrl-model/).

### Criterion P1.3.2

**Service Offering requires being operated by Service Offering Provider with a verified identity.**

Already fulfilled with [Criterion P1.1.3](#criterion-p113) and [Criterion P1.2.9](#criterion-p129).

### Criterion P1.3.3

**Service Offering must provide a conformant self-description.**

Already fulfilled with the [baseline](#baseline) engine implementation. 

### Criterion P1.3.4

**Self-Description attributes need to be consistent across linked Self-Descriptions.**

Already fulfilled with the [baseline](#baseline) engine implementation. 

### Criterion P1.3.5

**The Provider shall ensure that the Consumer uses a verified identity provided by the Federator.**

The Gaia-X Compliance enables federations to extend the list of default [Trust Anchors](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/#trust-framework-scope).

## Data Protection

### Criterion P2.1.1

**The Provider shall offer the ability to establish a contract under Union or EU/EEA/Member State law and specifically addressing GDPR requirements.**

Already fulfilled with [Criterion P1.1.2](#criterion-p112) and [Regulation (EU) 2016/679](https://eur-lex.europa.eu/eli/reg/2016/679/oj). 

### Criterion P2.1.2

**The Provider shall define the roles and responsibilities of each party.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.1.3

**The Provider shall clearly define the technical and organizational measures in accordance with the roles and responsibilities of the parties, including an adequate level of detail.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.2.1

**The Provider shall be ultimately bound to instructions of the Customer.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.2.2

**The Provider shall clearly define how Customer may instruct, including by electronic means such as configuration tools or APIs.**

The Gaia-X Compliance is **enabling** **attestation** of:

- service access point description on service offering, include API specification.

### Criterion P2.2.3

**The Provider shall clearly define if and to which extent third country transfer will take place.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.2.4

**The Provider shall clearly define if and to the extent third country transfers will take place, and by which means of Chapter V GDPR these transfers will be protected.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.2.5

**The Provider shall clearly define if and to which extent sub-processors will be involved.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.2.6

**The Provider shall clearly define if and to the extent sub-processors will be involved, and the measures that are in place regarding sub-processors management.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.2.7

**The Provider shall define the audit rights for the Customer.**

The Gaia-X Compliance is **enabling** **attestation** of:

- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.3.1

**In case of a joint controllership, the Provider shall ensure an arrangement pursuant to Art. 26 (1) GDPR is in place.**

The Gaia-X Compliance is **enabling** **attestation** of:

- description of sub and parent organisations.
- term&conditions and policy information on the Service Offering credential.

!! There is no template available to enable comparison across service offerings.

### Criterion P2.3.2

**In case of a joint controllership, at a minimum, the Provider shall ensure that the very essence of such agreement is communicated to data subjects.**

*This is not realistic to implement in most cases because the provider is unknown at the time of the data creation.*

*Suggested alternative is for the data suject to express permission, prohibition and duty when the data is being created or when a license right from the data owner is granted to the data provider*

### Criterion P2.3.3

**In case of a joint controllership, the Provider shall publish a point of contact for data subjects.**

The Gaia-X Compliance is **mandating** **declaration** of:

- data controller contact information in case the data product contain personal or sensitive information

## Cybersecurity

For all the Criterion P3.1.* in this section, the Gaia-X Compliance is **enabling** **attestation** of:

- the description of existing attestions for one or more identifiable objects of assessment.

## Portability

For all the Criterion P4.1.* in this section, the Gaia-X Compliance is **enabling** **attestation** of:

- the description of existing attestions for one or more identifiable objects of assessment.

## European Control

### Criterion P5.1.1

**For Label Level 2, the Provider shall provide the option that all Customer Data are processed and stored exclusively in EU/EEA.**

The Gaia-X Compliance is **mandating** **declaration** of:

- the country and administrative area information for data exchange services and its dependencies.

### Criterion P5.1.2

**For Label Level 3, the Provider shall process and store all Customer Data exclusively in the EU/EEA.**

The Gaia-X Compliance is **mandating** **declaration** of:

- the country and administrative area information for data exchange services and its dependencies.

### Criterion P5.1.3

**For Label Level 3, where the Provider or subcontractor is subject to legal obligations to transmit or disclose Customer Data on the basis of a non-EU/EEA statutory order, the Provider shall have verified safeguards in place to ensure that any access request is compliant with EU/EEA/Member State law.**

*not assessable*

### Criterion P5.1.4

**For Label Level 3, the Provider’s registered head office, headquarters and main establishment shall be established in a Member State of the EU/EEA.**

Already fulfilled with [Criterion P1.1.3](#criterion-p113) and the Gaia-X Compliance is **mandating** **declaration** of:

- the country and administrative area information for legal participant, ie including Providers, Consumers and Federators.

### Criterion P5.1.5

**For Label Level 3, Shareholders in the Provider, whose registered head office, headquarters and main establishment are not established in a Member State of the EU/EEA shall not, directly or indirectly, individually or jointly, hold control of the CSP. Control is defined as the ability of a natural or legal person to exercise decisive influence directly or indirectly on the CSP through one or more intermediate entities, de jure or de facto. (cf. Council Regulation No 139/2004 and Commission Consolidated Jurisdictional Notice under Council Regulation (EC) No 139/2004 for illustrations of decisive control).**

*To be implemented in a next release using the Ultimate Beneficial Owners (UBO) of legal entities*  
*To be re-assessed after the ruling from the ECJ on 22 November 2022*

### Criterion P5.1.6

**For Label Level 3, in the event of recourse by the Provider, in the context of the services provided to the Customer, to the services of a third-party company - including a subcontractor - whose registered head office, headquarters and main establishment is outside of the European Union or who is owned or controlled directly or indirectly by another third-party company registered outside the EU/EEA, the third-party company shall have no access over the Customer Data nor access and identity management for the services provided to the Customer. The Provider, including any of its sub-processor, shall push back any request received from non-European authorities to obtain communication of Customer Data relating to European Customers, except if request is made in execution of a court judgment or order that is valid and compliant under Union law and applicable Member States law as provided by Article 48 GDPR.**

*not assessable*

### Criterion P5.1.7

**For Label Level 3, the Provider must maintain continuous operating autonomy for all or part of the services it provides. The concept of operating autonomy shall be understood as the ability to maintain the provision of the cloud computing service by drawing on the provider’s own skills or by using adequate alternatives**

Already fulfilled with [Criterion P1.1.3](#criterion-p113), [Criterion P1.1.4](#criterion-p114), [Criterion P1.2.4](#criterion-p124), [Criterion P1.2.5](#criterion-p125) and [Criterion P2.3.1](#criterion-p231).

### Criterion P5.2.1

**The Provider shall not access Customer Data unless authorized by the Customer or when the access is in accordance with EU/EEA/Member State law.**

*To be implemented in a next release by describing data exchange service characteristics like homomorphic, federated learning, multi-party-computation or confidential computing*  
